/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import libraryproject.LibraryProject;

/**
 *
 * @author Peppa Point
 */
public class UserDao {

    public static boolean insert(User user) {
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "INSERT INTO user (\n"
                    + "                    loginName,\n"
                    + "                    password,\n"
                    + "                    name,\n"
                    + "                    surname,\n"
                    + "                    typeId\n"
                    + "                 )\n"
                    + "                 VALUES (\n"
                    + "                     '%s',\n"
                    + "                     '%s',\n"
                    + "                     '%s',\n"
                    + "                     '%s',\n"
                    + "                      %d\n"
                    + "                 );";
            stm.execute(String.format(sql, user.getLoginName(), user.getPassword(),
                    user.getName(), user.getSurname(), user.getTypeId()));
            Database.close();
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(UserDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return true;
    }

    public static boolean update(User user) {
        return true;
    }

    public static boolean delete(User user) {
        return true;
    }

    public static ArrayList<User> getUsers() {
        ArrayList<User> list = new ArrayList<>();
        Connection conn = Database.connect();
        try {
            Statement stm = conn.createStatement();
            String sql = "SELECT userId,\n"
                    + "       loginName,\n"
                    + "       password,\n"
                    + "       name,\n"
                    + "       surname,\n"
                    + "       typeId\n"
                    + "  FROM user";
            ResultSet rs = stm.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getInt("userId") + " " + rs.getString("loginName"));
                User user = toObject(rs);
                list.add(user);
            }
            Database.close();
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(LibraryProject.class.getName()).log(Level.SEVERE, null, ex);
        }
        Database.close();
        return null;
    }

    private static User toObject(ResultSet rs) throws SQLException {
        User user = new User();
        user.setUserId(rs.getInt("userId"));
        user.setLoginName(rs.getString("loginName"));
        user.setPassword(rs.getString("password"));
        user.setName(rs.getString("name"));
        user.setSurname(rs.getString("surname"));
        user.setTypeId(rs.getInt("typeId"));
        return user;
    }

    public static User getUser() {
        return null;
    }
}
