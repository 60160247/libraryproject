/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package libraryproject;

import database.User;
import database.UserDao;
import java.sql.Connection;
import java.util.ArrayList;

/**
 *
 * @author informatics
 */
public class LibraryProject {

    static String url = "jdbc:sqlite:./db/library.db";
    static Connection conn = null;

    public static void main(String[] args) {
        User newuser = new User();
        newuser.setLoginName("test1");
        newuser.setPassword("test1");
        newuser.setName("test1");
        newuser.setSurname("test1");
        newuser.setTypeId(1);
        UserDao.insert(newuser);

        ArrayList<User> list = UserDao.getUsers();
        for (User user : list) {
            System.out.println(user);
        }
    }

}
